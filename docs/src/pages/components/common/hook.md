# 组合式 API

## 介绍

目前项目中有封装一些可复用的 API，组件开发可以直接使用。

## API 列表

|名称|描述|
|---|---|
|useConfig|获取全局配置|
|useForm|表单下的子组件获取当前表单的一些参数|
|useLinkChildren|表单容器类组件获取子级集合|
|useFormEventEmit|表单下的组件统一触发事件|
