export enum DialogTypeEnum {
  Confirm = 'confirm',
  Alert = 'alert'
}
