# dl-cli 脚手架

为了方便快速初始化 DLUI 项目，可以直接使用此脚手架安装，内置 PC、移动端两套模板；

## 安装

```bash
# npm 安装
$ npm i @xuanmo/dl-cli -g
```

## 使用

1. 使用命令 `dl-cli create <prejectName>`
2. 根据选项选择模板，完成项目创建

## DLUI 官网

1. [DLUI](https://www.xuanmo.xin/-/dl-ui)
